export  default function slidersInit(){
	
	(function($){
	
	
    //slider on front_page
    $(".owl-1").owlCarousel({
        responsive: {
            0: {
                items: 3,
                stagePadding: 0
            },
            980: {
                items: 2,
                stagePadding: 20
            },
            // 1100: {
            //     items: 2,
            //     stagePadding: 80
            // },
            1360: {
                items: 2,
                stagePadding: 100
            },
            1600: {
                items: 3,
                stagePadding: 20
            }
        },
     
        loop: true,
        margin: 10,
        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 1000,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        dots: true,
        // autoHeight: true
    });






    //slider partnerzy

    $(".owl-2").owlCarousel({
        responsive: {
            0: {
                items: 3,
                margin: 40,
                nav: false
               
            },
            640: {
                items: 4,
                margin: 40,
                nav: false
            },
            830: {
                items: 4,
                margin: 60,
                nav: true,
            },
            1200: {
                items: 5,
                margin: 50
            }
        },
        loop: true,

        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 850,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        nav: true,
        // autoplayTimeout:1000,
        dots: false,
        autoHeight: true,
        	navText:[`<svg xmlns="http://www.w3.org/2000/svg" width="28" height="53" viewBox="0 0 28 53">
            <text id="_" data-name="&gt;" transform="translate(28 10) rotate(180)" fill="#b2b2b2" font-size="40" font-family="SegoeUI, Segoe UI"><tspan x="0" y="0">&gt;</tspan></text>
          </svg>`, `<svg xmlns="http://www.w3.org/2000/svg" width="28" height="53" viewBox="0 0 28 53">
          <text id="_" data-name="&gt;" transform="translate(0 43)" fill="#b2b2b2" font-size="40" font-family="SegoeUI, Segoe UI"><tspan x="0" y="-8">&gt;</tspan></text>
        </svg>`],
    });
		
	})(jQuery);
	
}
 