import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';



(function ($) {


	// $(document).foundation();

	$(document).ready(function () {
		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit();
		// $(window).resize(function() {
		// 	specialMenu()
		// })
		specialMenu();
		var navHeight = $("#navbar").height();
		//menu-sticky
		$(window).scroll(function () {
			if ($(window).width() >= 979) {
				if ($(this).scrollTop() > 1) {
					$('#navbar').addClass("sticky");
					$(".logo-box .image-logo").attr("src", "assets/img/LOGO MENU.svg");
				} else {
					$('#navbar').removeClass("sticky");
					$(".logo-box .image-logo").attr("src", "assets/img/Bez nazwy-1.svg");
				}
			}
		});


		// logo change on resize
		$(window).resize(function () {
			if ($(window).width() >= 979) {

				$(".logo-box .image-logo").attr("src", "assets/img/Bez nazwy-1.svg");
			} else {
				$(".logo-box .image-logo").attr("src", "assets/img/LOGO MENU.svg");
			}

		});


		//special-menu (opcje_leczenia, dzieci)
		function specialMenu() {
		
			if (!$('.special-menu').length) return; //nie odpalamy, gdy niepotrzebne
			var textH, vertOne, vert, fixmeTop, bottomPos, sectionHeight, dawnPosition, footerHeight;

			function calcValues() {
				textH = $(".special-menu").innerHeight();
				vertOne = $(window).height();
				vert = (vertOne - textH) / 2;
				fixmeTop = $('.special-menu').position().top - vert;
				footerHeight = $(".footer").height();
				bottomPos = $('body').height();
				sectionHeight = $('.section-height').height();
				dawnPosition = bottomPos - footerHeight - textH - sectionHeight + 50;
			}

			calcValues(); //initial
			$(window).resize(calcValues, scrollMenu); // update on resize
			// update on resize
			$(window).resize(function () {
				location.reload();
				calcValues();
				scrollMenu();
			})
			$(window).scroll(scrollMenu);

			function scrollMenu() { // assign scroll event listener
				var currentScroll = $(window).scrollTop() + 50; // get current position
				if (currentScroll >= fixmeTop && currentScroll < dawnPosition) { // position fixed
					$('.special-menu').addClass('fixed-class');
					$('.special-menu-box-box').removeClass('flex-down');
				} else if (currentScroll >= dawnPosition) { //remove fixed on the bottom
					$('.special-menu').removeClass('fixed-class');
					$('.special-menu-box-box').addClass('flex-down');
				} else { // remove fixed on the top
					$('.special-menu').removeClass('fixed-class');
					$('.special-menu-box-box').removeClass('flex-down');
				}
			}
		}


		//////////faq-ul-box
		faqMenu();

		function faqMenu() {
			if (!$('.faq-ul-box').length) return; //nie odpalamy, gdy niepotrzebne
			var bottomPos, dawnPosition, footerHeight, sectionHeight, textH;

			function calcValues() {
				textH = $(".faq-ul-box").innerHeight();
				footerHeight = $(".footer").height();
				bottomPos = $('body').height();
				sectionHeight = $('.section-height').height();
				dawnPosition = bottomPos - footerHeight - textH - sectionHeight;
			}
			calcValues(); //initial
			// $(window).resize(calcValues, scrollMenu); // update on resize
			$(window).scroll(scrollMenu);

			function scrollMenu() { // assign scroll event listener
				var currentScroll = $(window).scrollTop(); // get current position
				if (currentScroll >= dawnPosition) { //remove fixed on the bottom
					$('.faq-ul-box').css("position", 'static');
					$('.faq-menu').addClass('flex-end');

				} else { // remove fixed on the top
					$('.faq-ul-box').css("position", 'fixed');
					$('.faq-menu').removeClass('flex-end');
				}
			}
		}



		///care

		$(".care").click(function (e) {
			e.preventDefault()
			let id_faq = $(this).attr('data-scroll-care');
			let target = '.care-text-' + id_faq;
			$('.care').siblings().removeClass("add-orange");
			$(this).addClass("add-orange");

			$('html,body').animate({
				scrollTop: $(target).offset().top - navHeight
			}, 'slow');
		});


		///specjaliści

		$(".menu-specjalisci-option").click(function (e) {
			e.preventDefault()
			let id_faq = $(this).attr('data-show-spec');
			let target = '.specjalisci-' + id_faq;

			$('html,body').animate({
				scrollTop: $(target).offset().top - navHeight
			}, 'slow');
		});




		//faq-category

		$('.category-li').click(function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-category');
			let target = ".category-" + element_id;
			$(target).show();
			$(this).addClass("add-orange");
			$(this).siblings().removeClass("add-orange");
			$(target).siblings().hide();
		})

		//partnerzy

		$('.menu-wladze-big-option:first-child').addClass("add-orange");
		$('.menu-wladze-big-option').click(function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-menu');
			let target = ".menu-wladze-item-" + element_id;
			$('.menu-wladze-item').hide();
			$(target).show();
			$(this).addClass("add-orange ");
			$(this).siblings().removeClass("add-orange ");
			// $(target).siblings().hide();
		})

		// $('.menu-partnerzy-option:first-child').addClass("add-orange");
		$('body').on('click', '.menu-partnerzy-option', function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-item');
			let target = ".menu-partnerzy-item-" + element_id;
			$('.menu-partnerzy-item').hide();
			$(target).show();
			$(this).addClass("add-gray");
			$(this).siblings().removeClass("add-gray");
			// $(target).siblings().hide();
		})


		///body-animation-left
		$('.shadow-ball').click(function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-sympotm-left');
			let target = ".cell-left-" + element_id;
			$(target).show();
			$(this).addClass("shadow-ball-clicked");
			$(this).siblings().removeClass("shadow-ball-clicked");
			$(target).siblings().hide();
		})
		///body-animation-right
		$('.shadow-ball').click(function (e) {
			if ($(window).width() > 1024) {
				e.preventDefault()
				let element_id = $(this).attr('data-show-sympotm-right');
				let target = ".cell-right-" + element_id;
				$(target).show();
				$(this).addClass("shadow-ball-clicked");
				$(this).siblings().removeClass("shadow-ball-clicked");
				$(target).siblings().hide();

			} else {
				e.preventDefault()
				let element_id = $(this).attr('data-show-sympotm-right');
				let target = ".cell-left-" + element_id;
				$(target).show();
				$(this).addClass("shadow-ball-clicked");
				$(this).siblings().removeClass("shadow-ball-clicked");
				$(target).siblings().hide();
			}
		})

		//toggle answers= faq

		$('body').on('click', '.question', function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-answer');
			let target = ".answer-" + element_id;
			if ($(".answer:visible").not($(target)).length) {
				$(".answer").slideUp();
				$('.question').removeClass('rotate');
			}
			$(target).slideToggle();
			$(this).toggleClass('rotate');

		});



		//////////////////job-toggle
		$(".offer").click(function (e) {
			e.preventDefault()
			let id_faq = $(this).attr('data-show-offer');
			let target = '.offer-text-' + id_faq;
			if ($(".offer-text:visible").not($(target)).length) {
				$(".offer-text").slideUp();

				$(this).siblings().removeClass('offer-added-class rotate');
			}

			$(this).toggleClass('offer-added-class rotate');
			$(target).slideToggle();

			$('html,body').delay(500).animate({
				scrollTop: $('.offers-box').offset().top
			}, 'slow');
		});


	});

})(jQuery);